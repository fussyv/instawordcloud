import json
import time
import urllib.request
import re
from stop_words import substitutions

def start(instagramID):
	end_cursor = ''
	IGcontent = ''
	cc_count = 0
	url_host = 'https://www.instagram.com/'
	url_path = str(instagramID) + '/?__a=1'

	grapql_host = 'https://www.instagram.com/graphql/query/'
	url_path_with_id = ''

	try:
		url = url_host + url_path
		request = urllib.request.Request(url=url)
		response = urllib.request.urlopen(request,timeout=5)
		res_body = response.read()
		jsonPayload = json.loads(res_body.decode("utf-8"))
		targetUserId = jsonPayload['graphql']['user']["id"]
		isPrivate = jsonPayload['graphql']['user']["is_private"]
		url_path_with_id = '?query_hash=472f257a40c653c64c666ce877d59d2b&variables={"id":"' + targetUserId + '","first":50,"after":""}'
	except Exception as Q:		
		try:
			if (int(Q.getcode() / 100) == 4):
				print('User not found')
		except:
			print('Please check your network status')
		exit()

	while 1:
		try:
			url = grapql_host + url_path_with_id
			request = urllib.request.Request(url=url)
			response = urllib.request.urlopen(request,timeout=5)
			res_body = response.read()
			j = json.loads(res_body.decode("utf-8"))
			targetUser = j['data']['user']
			#print(str(targetUser))
			if isPrivate != True:
				items = targetUser['edge_owner_to_timeline_media']['edges']
				for item in items:
					edgeItems = item['node']['edge_media_to_caption']['edges']
					#print(str(edgeItems))
					for j in edgeItems:
						if 'node' in j:
							if 'text' in j['node']:
								#print(str(j['node']['text']))
								text_original = str(j['node']['text'])
								for x in substitutions:
									text_original = text_original.replace(x,substitutions[x])
								IGcontent += text_original

				#print(str(len(items)));
				if targetUser['edge_owner_to_timeline_media']['page_info']['has_next_page'] == True:
					end_cursor = targetUser['edge_owner_to_timeline_media']['page_info']['end_cursor']
					url_path_with_id = '?query_hash=472f257a40c653c64c666ce877d59d2b&variables={"id":"' + targetUserId + '","first":50,"after":"' + end_cursor + '"}'
					cc_count += len(items)
					stdout = '('
					stdout += str(int(cc_count / targetUser['edge_owner_to_timeline_media']['count'] * 100))
					stdout +='%)'

					stdout += ' Now query ' + str(cc_count) + ' posts '
					print(stdout)
					time.sleep(5)
				else:
					print('100%! Wait for rendering')
					return IGcontent            
			else:
				print('This user is private! Sorry!')
				exit()
		except Exception as Q:
			print(str(Q))
			exit()
			print('Time out? Wait 10 seconds (Use Ctrl-c to KeyboardInterrupt)')
			time.sleep(5)

def replace(string, substitutions):

	substrings = sorted(substitutions, key=len, reverse=True)
	regex = re.compile('|'.join(map(re.escape, substrings)))
	return regex.sub(lambda match: substitutions[match.group(0)], string)